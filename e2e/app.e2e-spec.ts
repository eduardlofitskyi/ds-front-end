import { DsFrontEndPage } from './app.po';

describe('ds-front-end App', () => {
  let page: DsFrontEndPage;

  beforeEach(() => {
    page = new DsFrontEndPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
