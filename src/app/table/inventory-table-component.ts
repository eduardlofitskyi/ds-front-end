import {Component, OnInit} from "@angular/core";
import {Listing} from "../entity/Listing";
import {ListingService} from "../service/ListingService";
import {EbayItem} from "../entity/EbayItem";

@Component({
  selector: 'inventory-table',
  templateUrl: './inventory-table-component.html'
  // styleUrls: ['./app.component.css']
})
export class InventoryTableComponent implements OnInit {

  displayDialog: boolean;
  displayDialogToAdd: boolean;

  listing: Listing = new Listing();

  selectedListing: Listing;

  newListing: boolean;

  listings: Listing[];

  constructor(private itemService: ListingService) {
  }

  ngOnInit() {
    this.itemService.getItems().then(items => this.listings = items);
    this.listing.item = new EbayItem();
  }

  showDialogToAdd() {
    this.newListing = true;
    this.listing = new Listing();
    this.listing.item = new EbayItem();
    this.displayDialog = true;
  }

  save() {
    let items = [...this.listings];
    if (this.newListing) {
      this.itemService.saveItem(this.listing)
        .then(item => {
          this.listings.push(item);
          this.listings = items;
          this.listing = null;
        })
    } else {
      items[this.findSelectedItemIndex()] = this.listing;
      this.itemService.updateItem(this.listing)
        .then(() => {
          this.listing = null;
        });
      this.listings = items;
    }
    this.displayDialog = false;
  }

  delete() {
    let index = this.findSelectedItemIndex();
    this.listings = this.listings.filter((val, i) => i != index);
    this.itemService.deleteItem(this.listing)
      .then(() => {
        this.listing = null;
      });
    this.displayDialog = false;
  }

  onRowSelect(event: any) {
    this.newListing = false;
    this.listing = this.cloneItem(event.data);
    this.displayDialog = true;
  }

  cloneItem(it: Listing): Listing {
    let item = new Listing();
    for (let prop in it) {
      item[prop] = it[prop];
    }
    return item;
  }

  findSelectedItemIndex(): number {
    return this.listings.indexOf(this.selectedListing);
  }
}

