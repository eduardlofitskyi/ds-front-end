import {EbayItem} from "./EbayItem";
export class Listing {
  id: String;
  sku: String;
  price: String;
  active: boolean;
  item: EbayItem;
}
