export class EbayItem {
  id: String;
  productId: String;
  price: String;
  shippingCost: String;
  name: String;
  link: String;
  imgLink: String;
}
