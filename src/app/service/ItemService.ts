import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Listing} from "../entity/Listing";

import "rxjs/add/operator/toPromise";
import {EbayItem} from "../entity/EbayItem";

@Injectable()
export class ItemService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private itemUrl = 'http://localhost:8081/supplier';  // URL to web api

  constructor(private http: Http) { }

  getItems(): Promise<Listing[]> {
    return this.http.get(this.itemUrl)
      .toPromise()
      .then(response => response.json()._embedded.listing as Listing[])
      .catch(this.handleError);
  }

  getItemsByLink(link: String): Promise<EbayItem[]> {
    return this.http.get(this.itemUrl + "/link?link=" + link)
      .toPromise()
      .then(response => response.json() as EbayItem[])
      .catch(this.handleError);
  }

  getItemByLink(): Promise<Listing[]> {
    return this.http.get(this.itemUrl)
      .toPromise()
      .then(response => response.json()._embedded.listing as Listing[])
      .catch(this.handleError);
  }

  saveItem(item: Listing): Promise<Listing>{
    let options = new RequestOptions({ headers: this.headers });

    return this.http.post(this.itemUrl, item, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  updateItem(listing: Listing): Promise<Listing>{
    let options = new RequestOptions({ headers: this.headers });

    return this.http.put(this.itemUrl + "/" + listing.id, listing, options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  deleteItem(listing: Listing): Promise<void> {
    console.log("dasda" + listing);
    return this.http.delete(this.itemUrl + "/" + listing.id, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
