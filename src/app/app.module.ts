import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {ListingService} from "./service/ListingService";
import {InventoryTableComponent} from "./table/inventory-table-component";
import {ButtonModule, DataTableModule, DialogModule, InputTextModule, MenubarModule, ContextMenuModule} from "primeng/primeng";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MenuComponent} from "./menu/menu.component";
import {CheckboxModule} from "primeng/primeng";
import {AddItemComponent} from "./search/add-item.component";
import {ItemService} from "./service/ItemService";

@NgModule({
  declarations: [
    AppComponent,
    InventoryTableComponent,
    MenuComponent,
    AddItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DataTableModule,
    DialogModule,
    BrowserAnimationsModule,
    InputTextModule,
    ButtonModule,
    MenubarModule,
    CheckboxModule,
    ContextMenuModule
  ],
  providers: [ListingService, ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
