import {MenuItem} from "primeng/primeng";
import {Component, OnInit} from "@angular/core";
import {ItemService} from "../service/ItemService";
import {EbayItem} from "../entity/EbayItem";

@Component({
  selector: 'add-item',
  templateUrl: './add-item.component.html'
  // styleUrls: ['./app.component.css']
})
export class AddItemComponent{
  itemLink: String;
  items: EbayItem[] = new EbayItem()[0];

  constructor(private itemService: ItemService) {}

  searchItems() {
    this.itemService.getItemsByLink(this.itemLink).then(items => this.items = items);
  }
}
