import { Component } from '@angular/core';
import {Http} from "@angular/http";
import {Listing} from "./entity/Listing";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  items: Listing[];

  constructor(private http: Http) { }

  onclick() {
    this.http.get("http://ds-stock-check-service-api-dev.herokuapp.com/check/price/up")
      .toPromise()
      .then(response => response.json() as Listing[]).then(items => this.items = items);
  }
}
